import { Injectable } from '@angular/core';
import * as JwtDecoder from 'jwt-decode';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class User {

  public jwt: string;

  constructor(private router: Router) { 
    this.jwt = localStorage.getItem('jwt');
  }

  public login(jwt: string){
    localStorage.setItem( 'jwt', jwt);
    this.jwt = jwt;
    this.navigateToApp();
  }

  logout(){
    localStorage.removeItem('jwt');
    this.jwt = null;
    this.navigateToLogin();
  }

  private get payload(){
    if(!this.jwt) return null;

    return JwtDecoder(this.jwt);
  }

  get getUser(){
    return this.payload['username'];
  }

  get getRole(): Role {
    return this.payload['role'];
  }

  navigateToLogin(){
    this.router.navigate(['/login']);
  }

  navigateToApp(){
    this.router.navigate(['/pokemons']);
  }
}

interface Role {
  id: number;
  name: string;
  code: string;
}
