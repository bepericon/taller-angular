import { Interceptor } from './interceptor';
import { PokemonsComponent } from './pokemons/pokemons.component';
import { AppRoutes } from './app.routes';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { PokefilterComponent } from './pokefilter/pokefilter.component';
import { PokelistComponent } from './pokelist/pokelist.component';
import { PokerowComponent } from './pokerow/pokerow.component';
import { PokepaginatorComponent } from './pokepaginator/pokepaginator.component';
import { PokeModalComponent } from './poke-modal/poke-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogoutComponent,
    PokemonsComponent,
    PokefilterComponent,
    PokelistComponent,
    PokerowComponent,
    PokepaginatorComponent,
    PokeModalComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    AppRoutes
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [PokeModalComponent]
})
export class AppModule { }
