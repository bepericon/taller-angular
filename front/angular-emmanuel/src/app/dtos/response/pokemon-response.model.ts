import { GenericResponse, GenericListResponse } from './generic.model';
import { Pokemon } from '../pokemon.model';


export class PokemonResponse extends GenericResponse<Pokemon>{}

export class PokemonListResponse extends GenericListResponse<Pokemon>{}