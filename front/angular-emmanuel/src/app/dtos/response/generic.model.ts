

export class GenericResponse<Data, Err = string> {
  
  data: Data;
  error: Err;
}

export class ListResponse<Item> {

  list: Array<Item>;
  offset: number;
  limit: number;
  total: number;
  
}

export class GenericListResponse<Item> extends GenericResponse<ListResponse<Item>> {}


