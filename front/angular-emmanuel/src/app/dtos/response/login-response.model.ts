import { GenericResponse } from './generic.model';


export class LoginResponse extends GenericResponse<string> {}