
export class PokeFilter{
  numero: number;
  nombre: string;
  limit: number;
  offset: number;

  getQueryParams(){
    return '?'+ 
      Object.keys(this)
        .filter(key => this[key] !== null)
        .map(key => `${key}=${this[key]}`)
        .join('&');
  }
}