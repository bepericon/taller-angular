import { PokeFilter } from './../dtos/pokefilter.model';
import { PokemonListResponse } from './../dtos/response/pokemon-response.model';
import { PokestorageService } from './../services/pokestorage.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pokelist',
  templateUrl: './pokelist.component.html',
  styleUrls: ['./pokelist.component.scss']
})
export class PokelistComponent implements OnInit {

  constructor(private pokeStorage: PokestorageService) { }

  ngOnInit() {
  }

  get pokeResponse$(): Observable<PokemonListResponse> {
    return this.pokeStorage.pokeResponse$;
  }
}
