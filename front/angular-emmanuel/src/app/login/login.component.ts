import { AuthService } from '../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public buttonText: string;
  loginForm: FormGroup;

  constructor(private authService: AuthService, private user: User) {
    this.buttonText = 'Login';

    this.loginForm = new FormGroup({
      username: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {
  }

  get invalidLogin():boolean { return this.loginForm.invalid; }

  login(){
    
    if(this.loginForm.invalid) {}

    var body = {
      username: 'admin',
      password: 'admin'
    }

    //Hacer post.
    this.authService.post(body)
      .subscribe(
        res => {
          this.user.login(res.data);  
        },
        error => {
          console.error(error);          
        }
      );
  }
}
