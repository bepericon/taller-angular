import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginResponse } from '../dtos/response/login-response.model';
import { User } from '../user';

const AUTH_URL: string = environment.url + 'auth/';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private user: User) { }

  post(body): Observable<LoginResponse>{
    return this.http.post<LoginResponse>( AUTH_URL + "login", body);
  }

  logout() {
    return this.http.post<LoginResponse>(AUTH_URL + "logout", {},
      {
        headers: {
          authorization: this.user.jwt
        }
      });
  }

}
