import { PokemonListResponse } from './../dtos/response/pokemon-response.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PokeFilter } from '../dtos/pokefilter.model';

@Injectable()
export class PokestorageService {

    pokeResponse$: Observable<PokemonListResponse>;
    pokeFilter: PokeFilter;

    constructor(){
        this.pokeFilter = new PokeFilter();
    }
}
