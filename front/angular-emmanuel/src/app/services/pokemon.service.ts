import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonListResponse, PokemonResponse } from '../dtos/response/pokemon-response.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PokeFilter } from '../dtos/pokefilter.model';

@Injectable()
export class PokemonService {

  constructor(private httpClient : HttpClient) { }

  getPokemons(filter: PokeFilter = new PokeFilter()): Observable<PokemonListResponse> {
    return this.httpClient
      .get<PokemonListResponse>(`${environment.url}pokemons${filter.getQueryParams()}`);
  }

  getPokemonById(id: number): Observable<PokemonResponse> {
    return this.httpClient
      .get<PokemonResponse>(`${environment.url}pokemons/${id}`);
  }
}