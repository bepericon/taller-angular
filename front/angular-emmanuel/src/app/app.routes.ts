import { AuthGuard } from './auth.guard';
import { PokemonsComponent } from './pokemons/pokemons.component';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { NoauthGuard } from './noauth.guard';

const appRoutes: Routes = [
	{ path: 'login', component: LoginComponent, canActivate: [NoauthGuard] },
	{ path: 'pokemons', component: PokemonsComponent, canActivate: [AuthGuard]},
	{ path: '**', redirectTo: 'login' }
];

export const AppRoutes = RouterModule.forRoot(appRoutes);