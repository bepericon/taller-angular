import { PokemonService } from './../services/pokemon.service';
import { PokestorageService } from './../services/pokestorage.service';
import { Component, OnInit } from '@angular/core';
import { PokeFilter } from '../dtos/pokefilter.model';

@Component({
  selector: 'app-pokefilter',
  templateUrl: './pokefilter.component.html',
  styleUrls: ['./pokefilter.component.scss']
})
export class PokefilterComponent implements OnInit {

  constructor(
    private pokeStorage: PokestorageService,
    private pokeService: PokemonService) { }

  ngOnInit() {
  }

  get filter(): PokeFilter{
    return this.pokeStorage.pokeFilter;
  }

  filtrar(){
    this.pokeStorage.pokeResponse$ = this.pokeService.getPokemons(this.filter);
  }

}
