import { PokemonService } from './../services/pokemon.service';
import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from '../dtos/pokemon.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PokeModalComponent } from '../poke-modal/poke-modal.component';

@Component({
  selector: '[app-pokerow]',
  templateUrl: './pokerow.component.html',
  styleUrls: ['./pokerow.component.scss']
})
export class PokerowComponent implements OnInit {

  @Input() pokeItem: Pokemon;

  constructor(
    private modalService: NgbModal,
    private pokeService: PokemonService) { }

  ngOnInit() {
  }

  ver() {
    const s = this.pokeService.getPokemonById(this.pokeItem.id)
      .subscribe(res => {
        const modalRef = this.modalService.open(PokeModalComponent);

        s.unsubscribe();

        modalRef.componentInstance.pokemon = res.data as Pokemon;

        modalRef.result
          .then(res => {
            console.log(res);
          })
          .catch(error => {
            console.log(error);
          });
      });
  }
}
