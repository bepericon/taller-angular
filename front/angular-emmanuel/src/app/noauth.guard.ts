import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class NoauthGuard implements CanActivate {
  constructor(private user: User){}

  canActivate(){
    if(this.user.jwt == null) {
      return true;
    }

    this.user.navigateToApp();
    return false;
  }
  
}
