import { User } from './user';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private user: User){}

  canActivate(){
    if(this.user.jwt != null) {
      return true;
    }

    this.user.navigateToLogin();
    return false;
  }
  
}
