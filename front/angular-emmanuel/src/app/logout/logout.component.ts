import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from '../user';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {
public logoutFallido: boolean;

  constructor(private authservice: AuthService,private user: User) { }

  ngOnInit() {
  }

  get falloLogin():boolean
{
  return this.logoutFallido;
}

  logout(){
    this.authservice.logout().subscribe(
      response => {
        this.logoutFallido = false;
        this.user.logout();  
      },
      error => {this.logoutFallido = true;}
    );
  }

}
