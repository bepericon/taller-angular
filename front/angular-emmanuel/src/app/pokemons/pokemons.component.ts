import { PokeModalComponent } from './../poke-modal/poke-modal.component';
import { Component, OnInit } from '@angular/core';
import { PokemonService } from '../services/pokemon.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PokestorageService } from '../services/pokestorage.service';
import { PokemonResponse, PokemonListResponse } from '../dtos/response/pokemon-response.model';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pokemons',
  templateUrl: './pokemons.component.html',
  styleUrls: ['./pokemons.component.scss'],
  providers: [ PokestorageService, PokemonService ]
})
export class PokemonsComponent implements OnInit {
  PokemonForm: FormGroup;

  constructor(
    private pokemonservice: PokemonService,
    private pokeStorage: PokestorageService) {
  }
  
  get pokemonID(): number {
    return 
  }

  ngOnInit() {
    this.pokeStorage.pokeResponse$ = this.pokemonservice.getPokemons();
  }

}
