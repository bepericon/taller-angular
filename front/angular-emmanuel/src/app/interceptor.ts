import { User } from './user';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Interceptor implements HttpInterceptor {
  
  constructor(private user: User) { }
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(! request.url.endsWith('login'))
      request = request.clone({ headers: request.headers.set('Authorization', this.user.jwt)});
    
    return next.handle(request);
  }
}
