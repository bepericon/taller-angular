import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokepaginatorComponent } from './pokepaginator.component';

describe('PokepaginatorComponent', () => {
  let component: PokepaginatorComponent;
  let fixture: ComponentFixture<PokepaginatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokepaginatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokepaginatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
